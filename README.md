Coding DOJO AngularJS e Gulp.js
===
Bem vindo ao Coding DOJO AngularJS e Gulp.js.

## Requisitos

### Objetivo
Criar um sistema de controle de tarefas.

### Regras de negócio
A tarefa deve conter id, nome.
O sistema deve possibilitar a criação, alteração, conclusão, listagem (ID e todos), e exclusão de tarefas.
Toda a comunicação com o backend deve ser feita utilizando APIs RESTFul.

## Como iniciar o desenvolvimento?

1. [Crie uma conta no Bitbucket.](https://bitbucket.org/account/signup/)
2. [Efetue um fork da branch master.](https://bitbucket.org/gsw-team/angular-gulp-dojo/fork)

## Iniciar o desenvolvimento

###### Clonar o seu repositório
```sh
git clone https://username@bitbucket.org/username/angular-gulp-dojo.git
```
*Substitua o **username** pelo seu usuário.*


###### Instalar as dependências
```sh
npm install bower gulp -g
npm install && bower install
```

###### Executar o projeto
```sh
gulp server
```