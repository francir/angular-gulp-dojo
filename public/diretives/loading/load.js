(function(angular){
  'use strict';
  var app = angular.module('angular-gulp-dojo');
  app.directive('loading', function(){
    return{
      restrict: 'E',
      scope:{
        msg: '@'
      },
      templateUrl: 'diretives/loading/loading.html'
    }
  })
})(angular);
