(function(angular){
    'use strict';
    var app = angular.module('angular-gulp-dojo');
    app.service('attrService', function(){
      var attrName;

      this.setAttrName = function(name){
        attrName = name;
      };

      this.getAttrName = function(){
        return attrName;
      };

    });
})(angular);
