(function(angular) {
  'use strict';
  var app = angular.module('angular-gulp-dojo');
  app.controller('TodoCtrl', function($scope, $http) {
    var url, config, vm = this;
    init();

    function init() {
      url = 'https://angular-gulp-dojo-backend.herokuapp.com/api/task/';
      config = { headers: { 'Content-Type': 'application/json' } };

      loadTasks();
      vm.focus = true;
      vm.newTask = {};
      vm.isNew = isNew;
      vm.saveOrUpdate = saveOrUpdate;
      vm.edit = edit;
      vm.remove = remove;
      vm.cancel = cancel;
      vm.cleanFocus = cleanFocus;
    }

    //TODO implementar http
    function loadTasks() {
      $http.get(url)
      .success(function(tasks) {
        vm.tasks = tasks;
      });
    }

    function isNew(task) {
      if (!task || !task._id) {
        return true;
      } else {
        return false;
      }
    }

    function saveOrUpdate(task) {
      if (!task) {
        return;
      }

      if (!task._id) {
        $http.post(url, task, config)
          .success(function() {
            loadTasks();
          });
      } else {
        $http.put(url, task, config)
          .success(function() {
            loadTasks();
          });
      }
      cancel();
    }

    function edit(task) {
      if (!task) {
        return;
      }
      vm.focus = true;
      vm.newTask = angular.copy(task);
    }

    function remove(task) {
      $http.delete(url+task._id)
        .success(function() {
            loadTasks();
        });
    }

    function cancel() {
      vm.newTask = {};
      vm.focus = false;
    }

    function cleanFocus() {
      vm.focus = false;
    }
  });
})(angular);
