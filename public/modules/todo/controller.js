(function(angular){
    'use strict';
    var app = angular.module('angular-gulp-dojo');
    app.controller('TodoController', function($scope, $http, attrService){

        var url='https://angular-gulp-dojo-backend.herokuapp.com/api/task/';
        loadTasks();
        $scope.airplane = false;
        $scope.attrName = attrService.getAttrName();

         function loadTasks() {
           $http.get(url)
           .success(function(tasks) {
             $scope.tasks = tasks;
             $scope.airplane = false;
           });
         }

         function cancel(){
           $scope.newtask = {};
           loadTasks();
         }

         $scope.save = function(task){
           $scope.airplane = true;
           if(!task._id){
             $http.post(url, task)
             .success(function(){
                $scope.newtask.name='';
                loadTasks();
             })
           }
           else{
             $http.put(url, task)
             .success(function(){
                $scope.newtask.name='';
                loadTasks();
             })
           }
           cancel();
         }

         $scope.delete = function(task){
           $scope.airplane = true;
           $http.delete(url+task._id)
           .success(function(){
              loadTasks();
              $scope.airplane = false;
           })
         }

         $scope.update = function(task){
           //console.log(task);
           $scope.newtask = {"name": task.name, "_id":task._id};
           //$scope.newtask = angular.copy(task);
         }

         $scope.cancel = cancel;
    });
})(angular);
