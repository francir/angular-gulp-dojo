(function(angular){
    'use strict';
    var app = angular.module('angular-gulp-dojo');
    app.controller('HomeCtrl', function($scope, $location, attrService){
      $scope.attrName = '';

      $scope.saveAttr = function (attrName){
        attrService.setAttrName(attrName);
        $location.path('/todo');
      }
    });
})(angular);
